﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class PlayerModel : MonoBehaviour
{
    public static int Personaje;
    public GameObject Personaje1, Personaje2, Personaje3;
    public float Speed;
    public Rigidbody rb;
    public float horizontal, vertical;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Speed = 100;
        
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
    }
}
