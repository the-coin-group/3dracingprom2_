﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject playermodel;
    public Rigidbody rb;
    public float _horizontal, _vertical;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        _horizontal = playermodel.gameObject.GetComponent<PlayerModel>().horizontal;
        _vertical = playermodel.gameObject.GetComponent<PlayerModel>().vertical;
        if (PlayerModel.Personaje == 0)
        {
            playermodel.gameObject.GetComponent<PlayerModel>().Speed = 100;
        }
        else if (PlayerModel.Personaje == 1)
        {
            playermodel.gameObject.GetComponent<PlayerModel>().Speed = 110;
        }
        else if (PlayerModel.Personaje == 2)
        {
            playermodel.gameObject.GetComponent<PlayerModel>().Speed = 120;
        }
        if (_horizontal > 0)
        {
            rb.velocity = new Vector3(_horizontal * playermodel.gameObject.GetComponent<PlayerModel>().Speed, 0, 0);
        }
        if (_horizontal < 0)
        {
            rb.velocity = new Vector3(_horizontal * playermodel.gameObject.GetComponent<PlayerModel>().Speed, 0, 0);
        }
        if (_vertical < 0)
        {
            rb.velocity = new Vector3(0, 0, _vertical * playermodel.gameObject.GetComponent<PlayerModel>().Speed);
        }
        if (_vertical > 0)
        {
            rb.velocity = new Vector3(0, 0, _vertical * playermodel.gameObject.GetComponent<PlayerModel>().Speed);
        }
    }
}
