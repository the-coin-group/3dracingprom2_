﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerView : MonoBehaviour
{
    public GameObject Personaje1, Personaje2, Personaje3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        ChangeChar();
    }

    public void personaje1()
    {
        Personaje1.SetActive(true);
        Personaje2.SetActive(false);
        Personaje3.SetActive(false);
    }
    public void personaje2()
    {
        Personaje1.SetActive(false);
        Personaje2.SetActive(true);
        Personaje3.SetActive(false);
    }
    public void personaje3()
    {
        Personaje1.SetActive(false);
        Personaje2.SetActive(false);
        Personaje3.SetActive(true);
    }

    void ChangeChar()
    {
        if (PlayerModel.Personaje==0)
        {
            Personaje1.SetActive(true);
            Personaje2.SetActive(false);
            Personaje3.SetActive(false);
        }
        else if (PlayerModel.Personaje == 1)
        {
            Personaje1.SetActive(false);
            Personaje2.SetActive(true);
            Personaje3.SetActive(false);
        }
        else if (PlayerModel.Personaje == 2)
        {
            Personaje1.SetActive(false);
            Personaje2.SetActive(false);
            Personaje3.SetActive(true);
        }
    }
}
