﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextoEscena : MonoBehaviour
{
    public Text MaxSpeed;
    public GameObject PLayerModel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MaxSpeed.text = "Velocidad: " + PLayerModel.gameObject.GetComponent<PlayerModel>().Speed;
    }
}
